# Bits & Bäume 2022: Was lernen wir aus Karten vom Internet?

> __Ausgehend vom „Critical Atlas of Internet“ von Louise Drul erkunden wir kartografische Darstellungen des Internets und was sie uns über das Internet sagen.__
> 
> Als gleichzeitig vereinfachte und verdichtete Darstellungen dieser Welt können uns Karten bei der Orientierung an unbekannten Orten helfen. Parallel waren sie auch immer Instrumente geopolitischer Herrschaftsansprüche, die die Annahmen derer enthalten und verbreiten, die sie konstruieren. Nicht anders verhält es sich mit Karten von virtuellen Räumen, was wir anhand kartografischer Darstellungen des Internets nachvollziehen wollen. Ausgehend von Arbeiten von Louise Drul, insbesondere dem „Critical Atlas of Internet“, machen wir uns ein Bild dieses abstrakten wie allgegenwärtigen Raums.
> 
> Wer sich schon immer gefragt hat, wie das Internet konstruiert ist, mag in diesem Kurzvortrag sowohl vertraute als auch unkonventionelle, aber hoffentlich immer einsichtsreiche Perspektiven auf diesen politischen und gestaltbaren Raum kennenlernen.

## Prerequisites

Install [R](https://www.r-project.org/).

### Setup Quarto

```r
# install R package
install.packages("quarto")

# download latest (nightly) quarto-cli
quarto_home <- path.expand(file.path("~", "quarto"))
asset_url <- "https://api.github.com/repos/quarto-dev/quarto-cli/releases" |>
  jsonlite::fromJSON() |>
  getElement("assets_url") |>
  getElement(1) |>
  jsonlite::fromJSON() |>
  dplyr::filter(grepl("-linux-amd64.tar.gz", browser_download_url)) |>
  getElement("browser_download_url")
asset_location <- tempfile(fileext = ".tar.gz")
download.file(url = asset_url, destfile = asset_location)
untar(tarfile = asset_location, exdir = quarto_home)
unlink(asset_location)
quarto_location <- file.path(quarto_home, list.files(quarto_home)[[1]], "bin")
write(paste0("PATH=", quarto_location, ":$PATH"),
      file = path.expand(file.path("~", ".profile")), append=TRUE)
Sys.setenv(PATH = paste(quarto_location, Sys.getenv("PATH"), sep = ":"))
```

## Rendering

```r
setwd("path/to/presentation")
quarto::quarto_render("index.qmd")
```
